package com.msfestimates.mobile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsfEstimatesApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsfEstimatesApplication.class, args);
	}

}
