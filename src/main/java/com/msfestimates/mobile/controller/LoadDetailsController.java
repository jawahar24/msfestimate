package com.msfestimates.mobile.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.msfestimates.mobile.model.LoadDetails;
import com.msfestimates.mobile.service.LoadDetailsService;

@RestController
@RequestMapping("/api/load")
public class LoadDetailsController 
{
	@Autowired
	private LoadDetailsService loadDetailsService;
	
	
	@PostMapping("/CreateorUpdateLoadDetails")
	public ResponseEntity<?> CreateOrUpdateLoadDetails(@RequestBody LoadDetails loadDetails)
	{
		ResponseEntity<?> createOrUpdateloadDetails = loadDetailsService.CreateOrUpdateLoadDetails(loadDetails);
		return createOrUpdateloadDetails;
	}
	
	@GetMapping("/get/{loadId}")
	public ResponseEntity<?> getLoadDetails(@PathVariable Long loadId)
	{
		ResponseEntity<?> getLoadDetails = loadDetailsService.getLoadDetailsById(loadId);
		return getLoadDetails;
	}
	
	@GetMapping("/getLoads")
	public ResponseEntity<?> getListOfLoadDetails()
	{
		ResponseEntity<?> getLoadList = loadDetailsService.getAllLoadDetails();
		return getLoadList;
	}
}
