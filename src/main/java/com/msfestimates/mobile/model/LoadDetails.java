package com.msfestimates.mobile.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.lang.NonNull;
import org.springframework.lang.NonNullFields;

@Entity
@Table(name = "LOAD_DETAILS")
public class LoadDetails 
{
	@Id
	@SequenceGenerator(name = "LOAD_DETAILS_SEQ", sequenceName = "LOAD_DETAILS_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE ,generator = "LOAD_DETAILS_SEQ")
	@Column(name = "LOAD_ID")
	private Long loadId;
	
	@Column(name = "DATE")
	private Date date;
	
	@Column(name = "VehicleNumber")
	private String vehicleNumber;
	
	
	@Column(name = "AGENT")
	private String agent;
	
	@Column(name = "VARIETY")
	private String variety;
	
	
	@Column(name = "GROSSWEIGHT")
	private Double grossWeight;
	

	
	@Column(name = "TAREWEIGHT")
	private Double tareWeight;
	
	@Column(name = "POINTRATE")
	private Double pointRate;
	
	@Column(name = "POINT")
	private Double point;
	
	@Column(name = "NETWEIGH_KGS_PER_TON")
	private Double netweighperTon;
	
	@Column(name = "NETWEIGH")
	private Double netWeight;
	
	@Column(name = "TONRATE")
	private Double tonRate;
	
	@Column(name = "BAGS")
	private Long bags;
	
	@Column(name = "COMMISION")
	private Double commission;
	
	@Column(name = "UNLOADING")
	private Double unloading;
	
	@Column(name = "TOTALPRICE")
	private Double totalAmount;
	
	
		// GETTERS AND SETTERS
	
	
	
	public Long getLoadId() {
		return loadId;
	}

	public void setLoadId(Long loadId) {
		this.loadId = loadId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public String getVariety() {
		return variety;
	}

	public void setVariety(String variety) {
		this.variety = variety;
	}

	public Double getTareWeight() {
		return tareWeight;
	}

	public void setTareWeight(Double tareWeight) {
		this.tareWeight = tareWeight;
	}

	public Double getPointRate() {
		return pointRate;
	}

	public void setPointRate(Double pointRate) {
		this.pointRate = pointRate;
	}

	public Double getPoint() {
		return point;
	}

	public void setPoint(Double point) {
		this.point = point;
	}

	public Double getNetWeight() {
		return netWeight;
	}

	public void setNetWeight(Double netWeight) {
		this.netWeight = netWeight;
	}

	

	

	public Double getTonRate() {
		return tonRate;
	}

	public void setTonRate(Double tonRate) {
		this.tonRate = tonRate;
	}

	public Long getBags() {
		return bags;
	}

	public void setBags(Long bags) {
		this.bags = bags;
	}

	public Double getCommission() {
		return commission;
	}

	public void setCommission(Double commission) {
		this.commission = commission;
	}

	

	public Double getUnloading() {
		return unloading;
	}

	public void setUnloading(Double unloading) {
		this.unloading = unloading;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Double getGrossWeight() {
		return grossWeight;
	}

	public void setGrossWeight(Double grossWeight) {
		this.grossWeight = grossWeight;
	}

	// CONSTRUCTOR 
	
	public Double getNetweighperTon() {
		return netweighperTon;
	}

	public void setNetweighperTon(Double netweighperTon) {
		this.netweighperTon = netweighperTon;
	}

	public LoadDetails() {
		super();
		// TODO Auto-generated constructor stub
	}
	
			
	
	
}
