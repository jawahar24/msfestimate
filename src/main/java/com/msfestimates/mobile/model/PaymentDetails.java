package com.msfestimates.mobile.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "PAYMENT_DETAILS")
public class PaymentDetails 
{
	@Id
	@SequenceGenerator(name = "PAYMENT_DETAILS_SEQ", sequenceName = "PAYMENT_DETAILS_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE ,generator = "PAYMENT_DETAILS_SEQ")
	@Column(name = "PAYMENT_ID")
	private Long paymentId;
	
	@Column(name = "PAID_DATE")
	private Date paidDate;
	
	@Column(name = "AMOUNT_PAID")
	private Double paidAmount;
	
	@Column(name = "BAL_AMOUNT")
	private Double balAmount;
	
	
	@OneToMany(targetEntity = LoadDetails.class,cascade = CascadeType.ALL)
    @JoinColumn(name = "agentDetails",referencedColumnName = "PAYMENT_ID")
	private List<LoadDetails> agentLoadDetails;
	
	
	public PaymentDetails() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Long getPaymentId() {
		return paymentId;
	}


	public void setPaymentId(Long paymentId) {
		this.paymentId = paymentId;
	}


	public Date getPaidDate() {
		return paidDate;
	}


	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}


	public Double getPaidAmount() {
		return paidAmount;
	}


	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}


	public Double getBalAmount() {
		return balAmount;
	}


	public void setBalAmount(Double balAmount) {
		this.balAmount = balAmount;
	}


	public List<LoadDetails> getAgentLoadDetails() {
		return agentLoadDetails;
	}


	public void setAgentLoadDetails(List<LoadDetails> agentLoadDetails) {
		this.agentLoadDetails = agentLoadDetails;
	}

	
}
