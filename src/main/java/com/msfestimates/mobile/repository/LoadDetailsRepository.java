package com.msfestimates.mobile.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.msfestimates.mobile.model.LoadDetails;

@Repository
public interface LoadDetailsRepository extends JpaRepository<LoadDetails, Long>
{

	@Query(value = "SELECT * FROM LOAD_DETAILS WHERE LOAD_ID =:loadId", nativeQuery = true)
	LoadDetails findLoadDetailsById(@Param("loadId") Long loadId);
	
}
