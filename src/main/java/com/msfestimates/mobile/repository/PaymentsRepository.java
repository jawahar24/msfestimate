package com.msfestimates.mobile.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.msfestimates.mobile.model.PaymentDetails;

@Repository
public interface PaymentsRepository extends JpaRepository<PaymentDetails, Long> {

}
