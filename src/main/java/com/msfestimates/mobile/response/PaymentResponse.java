package com.msfestimates.mobile.response;

import java.util.Date;

public class PaymentResponse 
{
	private String agentName;
	
	private Date paymentDate;
	
	private Double paidAmount;
	
	private Double balAmount;
	
	private Double netAmount;

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public Double getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}

	public Double getBalAmount() {
		return balAmount;
	}

	public void setBalAmount(Double balAmount) {
		this.balAmount = balAmount;
	}

	public Double getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(Double netAmount) {
		this.netAmount = netAmount;
	}
	
	
	
}
