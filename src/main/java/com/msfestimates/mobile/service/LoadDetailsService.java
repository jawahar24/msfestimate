package com.msfestimates.mobile.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.msfestimates.mobile.exception.IdNotFoundException;
import com.msfestimates.mobile.model.LoadDetails;
import com.msfestimates.mobile.repository.LoadDetailsRepository;
import com.msfestimates.mobile.response.ApiResponse;

@Service
public class LoadDetailsService 
{
	@Autowired
	private LoadDetailsRepository loadDetailsRepository;
	
	Logger logs = LoggerFactory.getLogger(LoadDetailsService.class);
	
			// CREATE OR UPDATE LOAD DETAILS
	
	public ResponseEntity<?> CreateOrUpdateLoadDetails(LoadDetails loadDetails)
	{
		try
		{
			Long loadId = loadDetails.getLoadId();
			
			String value = "";
			
			if(loadDetails.getAgent() != null)
				value = "agent";
			
			if(loadDetails.getGrossWeight()!= null)
				value = "grossWeight";
			
			if(loadDetails.getTareWeight()!=null)
				value = "tareWeight";
			
			if(loadDetails.getPoint()!=null)
				value = "point";
			
			if(loadDetails.getPointRate()!= null)
				value = "pointRate";
			
			if(loadDetails.getVariety()!= null)
				value = "variety";
			
			if(loadId != null)
			{
				LoadDetails isLoadDetails = loadDetailsRepository.findLoadDetailsById(loadId);
				
				if(isLoadDetails == null)
				{
					return new ResponseEntity(new ApiResponse(false, "No Load Details Found"),
							HttpStatus.BAD_REQUEST);
				}
				
			}
			
			Double netWeightCals = loadDetails.getGrossWeight() - loadDetails.getTareWeight();
			
			Double weight50kgintoTon = netWeightCals - (netWeightCals * 50 /1000);
			
			Double tonRate = loadDetails.getPoint() * loadDetails.getPointRate();
			
			Long bagsNumber = (long) ((netWeightCals - (netWeightCals * 50 / 1000))/75);
			
			Double commision = weight50kgintoTon * 100 / 1000;
			
			Double unloading = weight50kgintoTon * 30 / 1000;
			
			Double totalAmount = ((loadDetails.getPointRate() * loadDetails.getPoint() * weight50kgintoTon)/1000) - unloading + commision;
			
			loadDetails.setNetWeight(netWeightCals);
			
			loadDetails.setNetweighperTon(weight50kgintoTon);
			
			loadDetails.setTonRate(tonRate);
			
			loadDetails.setBags(bagsNumber);
			
			loadDetails.setCommission(commision);
			
			loadDetails.setUnloading(unloading);
			
			loadDetails.setTotalAmount(totalAmount);
			
			loadDetailsRepository.save(loadDetails);
			
			
		} catch(Exception e)
		{
			logs.error("Unable to submit load details" + e);
			
			return new ResponseEntity(new ApiResponse(false, "Unable to Save Load Details")
					,HttpStatus.BAD_GATEWAY);
		}
		
		return new ResponseEntity(loadDetails, HttpStatus.CREATED);
		
	}
	
	
				// GET LOAD DETAILS BY ID
	
	public ResponseEntity<LoadDetails> getLoadDetailsById(Long loadId)
	{
		LoadDetails getloadDetails = loadDetailsRepository.findById(loadId).orElseThrow( () -> new IdNotFoundException("Id Not Found"));
		
		return new ResponseEntity(getloadDetails, HttpStatus.OK);
	}
	
	
			// GET ALL LOAD DETAILS 
	
	public ResponseEntity<LoadDetails> getAllLoadDetails()
	{
		List<LoadDetails> loads;

		try {
			loads = loadDetailsRepository.findAll();

			if (loads == null) {
				loads = new ArrayList<LoadDetails>();
			}
		} catch (Exception e) {
			
			logs.error("***!!! not able to fetch Groups!!! ***");

			return new ResponseEntity(new ApiResponse(false, "Facing issue to get List of Load Details !"),
					HttpStatus.BAD_REQUEST);

		}
		return new ResponseEntity(loads, HttpStatus.OK);
	}
}
